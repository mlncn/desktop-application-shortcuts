Shortcuts to run a command as if it were an application in Ubuntu.

These are program-launching scripts prettified with pictures and made findable by Ubuntu/PopOS's built-in launcher by creating .desktop files.

This repository is the entire contents of my `~/.local/share/applications` directory.  Frustratingly, references to scripts and icons must be absolute to work in .desktop files, so unless your local computer user name is mlncn you'll have to make your own modifications.  The gist of it should be quite clear with this repository.

Not sure if UnityLauncher is still a thing, but this documentation generally works on Ubuntu and worked on Mint Debian edition also.

https://help.ubuntu.com/community/UnityLaunchersAndDesktopFiles
